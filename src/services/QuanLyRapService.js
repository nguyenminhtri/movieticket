/* eslint-disable no-useless-constructor */
import { BaseService } from "./BaseServices";
export class QuanLyRapService extends BaseService {
  constructor() {
    super();
  }

  layDanhSachCumRap = () => {
    return this.get(`api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP01`);
  };
  LayThongTinHeThongRap = () => {
    return this.get(`/api/QuanLyRap/LayThongTinHeThongRap`);
  };
  LayThongTinCumRap = (maHeThongRap) => {
    return this.get(
      `api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=${maHeThongRap}`
    );
  };
  layThongTinLichChieuPhim = (maPhim) => {
    return this.get(`api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`);
  };
}
export const quanLyRapService = new QuanLyRapService();
