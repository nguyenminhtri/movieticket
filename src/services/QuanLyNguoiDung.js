/* eslint-disable no-useless-constructor */
import { BaseService } from "./BaseServices";
export class QuanLyNguoiDungServices extends BaseService {
  constructor() {
    super();
  }

  dangNhap = (thongTinDangNhap) => {
    return this.post(`api/QuanLyNguoiDung/DangNhap`, thongTinDangNhap);
  };
  dangKi = (taiKhoanDangKi) => {
    return this.post(`api/QuanLyNguoiDung/DangKy`, taiKhoanDangKi);
  };
  layThongTinNguoiDung = () => {
    return this.post(`api/QuanLyNguoiDung/ThongTinTaiKhoan`);
  };
}
export const quanLyNguoiDungServices = new QuanLyNguoiDungServices();
