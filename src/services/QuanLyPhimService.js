/* eslint-disable no-useless-constructor */
import { BaseService } from "./BaseServices";
export class QuanLyPhimService extends BaseService {
  constructor() {
    super();
  }

  layDanhSachBanner = () => {
    return this.get(`api/QuanLyPhim/LayDanhSachBanner`);
  };
  layDanhSachPhim = (tenPhim = "") => {
    if (tenPhim !== "") {
      return this.get(
        `/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05&tenPhim=${tenPhim}`
      );
    } else {
      return this.get(`api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05`);
    }
  };
  themPhimUploadHinh = (formData) => {
    return this.post(`/api/QuanLyPhim/ThemPhimUploadHinh`, formData);
  };
  layThongTinPhim = (maPhim) => {
    return this.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`);
  };
  CapNhatPhimUpload = (formData) => {
    return this.post(`/api/QuanLyPhim/CapNhatPhimUpload`, formData);
  };
  xoaPhim = (maPhim) => {
    return this.delete(`/api/QuanLyPhim/XoaPhim?MaPhim=${maPhim}`);
  };
}
export const quanLyPhimService = new QuanLyPhimService();
