/* eslint-disable no-useless-constructor */
import { BaseService } from "./BaseServices";
export class QuanLyDatVeServices extends BaseService {
  constructor() {
    super();
  }

  layChiTietPhongVe = (maLichChieu) => {
    return this.get(
      `api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`
    );
  };
  datVe = (thongTinDatVe) => {
    return this.post(`api/QuanLyDatVe/DatVe`, thongTinDatVe);
  };
  taoLichChieu = (thongTinLichChieu) => {
    return this.post(`api/QuanLyDatVe/TaoLichChieu`, thongTinLichChieu);
  };
}
export const quanLyDatVeServices = new QuanLyDatVeServices();
