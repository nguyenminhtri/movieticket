import { quanLyRapService } from "../../services/QuanLyRapService";
import { SET_CHI_TIET_PHIM, SET_HE_THONG_RAP } from "../types/Type";

export const layDanhSachRapAction = () => {
  return async (dispatch) => {
    try {
      const result = await quanLyRapService.layDanhSachCumRap();

      dispatch({
        type: SET_HE_THONG_RAP,
        payload: result.data.content,
      });
    } catch (error) {
      console.log(error);
    }
  };
};
export const LayThongTinCumRapAction = (maHeThongRap) => {
  return async (dispatch) => {
    try {
      const result = await quanLyRapService.LayThongTinCumRap(maHeThongRap);
    } catch (error) {
      console.log(error);
    }
  };
};
export const layThongTinChiTietPhim = (id) => {
  return async (dispatch) => {
    try {
      const result = await quanLyRapService.layThongTinLichChieuPhim(id);
      dispatch({
        type: SET_CHI_TIET_PHIM,
        payload: result.data.content,
      });
    } catch (error) {
      console.log(error);
    }
  };
};
