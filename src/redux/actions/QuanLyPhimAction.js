import { message } from "antd";
import { quanLyPhimService } from "../../services/QuanLyPhimService";
import { SET_DANH_SACH_PHIM, SET_THONG_TIN_PHIM } from "../types/Type";
export const layDanhSachPhimAction = (tenPhim) => {
  return async (dispatch) => {
    try {
      const result = await quanLyPhimService.layDanhSachPhim(tenPhim);

      dispatch({
        type: SET_DANH_SACH_PHIM,
        payload: result.data.content,
      });
    } catch (error) {
      console.log(error);
    }
  };
};
export const themPhimUploadHinhAction = (formData) => {
  return async (dispatch) => {
    try {
      const result = await quanLyPhimService.themPhimUploadHinh(formData);
      console.log("result: ", result);
    } catch (error) {
      console.log(error);
    }
  };
};
export const layThongTinPhimAction = (maPhim) => {
  return async (dispatch) => {
    try {
      const result = await quanLyPhimService.layThongTinPhim(maPhim);

      dispatch({
        type: SET_THONG_TIN_PHIM,
        payload: result.data.content,
      });
    } catch (error) {
      console.log(error);
    }
  };
};
export const CapNhatPhimUploadAction = (formData, navigate) => {
  return async (dispatch) => {
    try {
      const result = await quanLyPhimService.CapNhatPhimUpload(formData);
      navigate("/admin/films");
      dispatch(layThongTinPhimAction());
    } catch (error) {
      console.log(error);
    }
  };
};
export const xoaPhimAction = (maPhim) => {
  return async (dispatch) => {
    try {
      const result = await quanLyPhimService.xoaPhim(maPhim);
      await message.success("Xóa phim thành công");
      dispatch(layDanhSachPhimAction());
    } catch (error) {
      console.log(error);
    }
  };
};
