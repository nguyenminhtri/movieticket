import { quanLyPhimService } from "../../services/QuanLyPhimService";
import { SET_CAROUSEL } from "../types/Type";
export const getCarouselAction = () => {
  return async (dispatch) => {
    try {
      const result = await quanLyPhimService.layDanhSachBanner();

      dispatch({
        type: SET_CAROUSEL,
        payload: result.data.content,
      });
    } catch (error) {
      console.log(error);
    }
  };
};
