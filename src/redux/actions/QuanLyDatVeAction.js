import { quanLyDatVeServices } from "../../services/QuanLyDatVeService";
import {
  CHUYEN_TAB,
  DAT_VE_HOAN_TAT,
  DISPLAY_LOADING,
  HIDE_LOADING,
  LAY_CHI_TIET_PHONG_VE,
} from "../types/Type";

export const layChiTietPhongVeAction = (maLichChieu) => {
  return async (dispatch) => {
    try {
      const result = await quanLyDatVeServices.layChiTietPhongVe(maLichChieu);

      dispatch({
        type: LAY_CHI_TIET_PHONG_VE,
        payload: result.data.content,
      });
    } catch (error) {
      console.log(error);
    }
  };
};
export const datVeAction = (thongTinDatVe) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: DISPLAY_LOADING,
      });
      await dispatch(layChiTietPhongVeAction(thongTinDatVe.maLichChieu));
      await dispatch({ type: DAT_VE_HOAN_TAT });
      await dispatch({
        type: HIDE_LOADING,
      });
      dispatch({ type: CHUYEN_TAB });
    } catch (error) {
      console.log(error);
    }
  };
};
