import { quanLyNguoiDungServices } from "../../services/QuanLyNguoiDung";
import { DANG_NHAP, SET_THONG_TIN_NGUOI_DUNG } from "../types/Type";
import { message } from "antd";
export const dangNhapAction = (values, navigate) => {
  return async (dispatch) => {
    try {
      const result = await quanLyNguoiDungServices.dangNhap(values);
      dispatch({
        type: DANG_NHAP,
        payload: result.data.content,
      });
      navigate();
      message.success("Đăng nhập thành công");
    } catch (error) {
      message.error("Tài khoản hoặc mật khẩu không đúng");
    }
  };
};
export const dangKiAction = (taiKhoanDangKi, navigate) => {
  return async (dispatch) => {
    try {
      const result = await quanLyNguoiDungServices.dangKi(taiKhoanDangKi);
      console.log("result: ", result);
      dispatch({
        type: DANG_NHAP,
        payload: result.data.content,
      });
      navigate();
      message.success("Đăng kí tài khoản thành công");
    } catch (error) {
      console.log(error.response.data.content);

      message.error("Tài khoản hoặc mật khẩu không đúng");
    }
  };
};
export const layThongTinNguoiDungAction = (thongTinDangNhap) => {
  return async (dispatch) => {
    try {
      const result = await quanLyNguoiDungServices.layThongTinNguoiDung(
        thongTinDangNhap
      );

      dispatch({
        type: SET_THONG_TIN_NGUOI_DUNG,
        payload: result.data.content,
      });
    } catch (error) {
      console.log(error);
    }
  };
};
