import { ThongTinLichChieu } from "../../_core/models/ThongTinPhongVe";
import {
  CHUYEN_TAB,
  DAT_VE,
  DAT_VE_HOAN_TAT,
  LAY_CHI_TIET_PHONG_VE,
  RETURN_TAB,
} from "../types/Type";

const initialState = {
  chiTietPhongVe: new ThongTinLichChieu(),
  danhSachGheDangDat: [],
  tabActive: "1",
  danhSachGheKhachDat: [{ maGhe: 88690 }, { maGhe: 88691 }],
};

export const QuanLyDatVeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case LAY_CHI_TIET_PHONG_VE:
      state.chiTietPhongVe = payload;

      return { ...state };

    case DAT_VE:
      let danhSachGheCapNhap = [...state.danhSachGheDangDat];

      let index = danhSachGheCapNhap.findIndex((item) => {
        return item.maGhe === payload.maGhe;
      });

      if (index !== -1) {
        danhSachGheCapNhap.splice(index, 1);
      } else {
        danhSachGheCapNhap.push(payload);
      }

      return { ...state, danhSachGheDangDat: danhSachGheCapNhap };
    case DAT_VE_HOAN_TAT: {
      state.danhSachGheDangDat = [];
      return { ...state };
    }
    case CHUYEN_TAB: {
      state.tabActive = "2";
      return { ...state };
    }
    case RETURN_TAB: {
      console.log(payload);

      state.tabActive = payload;
      return { ...state };
    }
    default:
      return state;
  }
};
