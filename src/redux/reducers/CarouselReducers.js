import { SET_CAROUSEL } from "../types/Type";

/* eslint-disable import/no-anonymous-default-export */
const initialState = {
  arrImg: [],
};

export const CarouselReducers = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_CAROUSEL:
      state.arrImg = payload;
      return { ...state };

    default:
      return state;
  }
};
