import { SET_HE_THONG_RAP } from "../types/Type";

const initialState = {
  heThongRapChieu: [],
};

export const QuanLyRapReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_HE_THONG_RAP:
      state.heThongRapChieu = payload;

      return { ...state, ...payload };

    default:
      return state;
  }
};
