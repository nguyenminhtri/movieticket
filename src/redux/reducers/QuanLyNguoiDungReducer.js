import { DANG_NHAP, SET_THONG_TIN_NGUOI_DUNG, USER_LOGIN } from "../types/Type";
let user = null;
if (localStorage.getItem(USER_LOGIN)) {
  user = JSON.parse(localStorage.getItem(USER_LOGIN));
}

const initialState = {
  userLogin: user,
  thongTinNguoiDung: {},
};

export const QuanLyNguoiDungReducer = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case DANG_NHAP:
      localStorage.setItem(USER_LOGIN, JSON.stringify(payload));
      state.userLogin = payload;
      return { ...state };
    case SET_THONG_TIN_NGUOI_DUNG:
      state.thongTinNguoiDung = payload;
      return { ...state };
    default:
      return { ...state };
  }
};
