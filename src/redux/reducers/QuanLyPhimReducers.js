import {
  SET_CHI_TIET_PHIM,
  SET_DANH_SACH_PHIM,
  SET_PHIM_DANG_CHIEU,
  SET_PHIM_SAP_CHIEU,
  SET_THONG_TIN_PHIM,
} from "../types/Type";

/* eslint-disable import/no-anonymous-default-export */
const initialState = {
  arrPhim: [
    {
      maPhim: 11240,
      tenPhim: "Guardians of the Galaxy Vol. 3 ",
      biDanh: "guardians-of-the-galaxy-vol-3",
      trailer:
        "https://www.youtube.com/watch?v=QYEhSObq4PU&ab_channel=MarvelAustralia%26NewZealand",
      hinhAnh:
        "https://movienew.cybersoft.edu.vn/hinhanh/guardians-of-the-galaxy-vol-123_gp01.jpg",
      moTa: "Guardians of the Galaxy Vol. 3 (stylized as Guardians of the Galaxy Volume 3) is an upcoming American superhero film based on the Marvel Comics superhero team Guardians of the Galaxy, produced by Marvel Studios and distributed by Walt Disney Studios Motion Pictures. It is intended to be the sequel to Guardians of the Galaxy (2014) and Guardians of the Galaxy Vol. 2 (2017) and the 32nd film in the Marvel Cinematic Universe (MCU). The film is written and directed by James Gunn and stars an ensemble cast featuring Chris Pratt, Zoe Saldaña, Dave Bautista, Karen Gillan, Pom Klementieff, Vin Diesel, Bradley Cooper, Will Poulter, Elizabeth Debicki, and Sylvester Stallone. In the film, the Guardians embark on a mission to defend the universe and protect one of their own.",
      maNhom: "GP01",
      ngayKhoiChieu: "2022-12-19T14:52:02.9",
      danhGia: 10,
      hot: true,
      dangChieu: true,
      sapChieu: true,
    },
    {
      maPhim: 11240,
      tenPhim: "Guardians of the Galaxy Vol. 3 ",
      biDanh: "guardians-of-the-galaxy-vol-3",
      trailer:
        "https://www.youtube.com/watch?v=QYEhSObq4PU&ab_channel=MarvelAustralia%26NewZealand",
      hinhAnh:
        "https://movienew.cybersoft.edu.vn/hinhanh/guardians-of-the-galaxy-vol-123_gp01.jpg",
      moTa: "Guardians of the Galaxy Vol. 3 (stylized as Guardians of the Galaxy Volume 3) is an upcoming American superhero film based on the Marvel Comics superhero team Guardians of the Galaxy, produced by Marvel Studios and distributed by Walt Disney Studios Motion Pictures. It is intended to be the sequel to Guardians of the Galaxy (2014) and Guardians of the Galaxy Vol. 2 (2017) and the 32nd film in the Marvel Cinematic Universe (MCU). The film is written and directed by James Gunn and stars an ensemble cast featuring Chris Pratt, Zoe Saldaña, Dave Bautista, Karen Gillan, Pom Klementieff, Vin Diesel, Bradley Cooper, Will Poulter, Elizabeth Debicki, and Sylvester Stallone. In the film, the Guardians embark on a mission to defend the universe and protect one of their own.",
      maNhom: "GP01",
      ngayKhoiChieu: "2022-12-19T14:52:02.9",
      danhGia: 10,
      hot: true,
      dangChieu: true,
      sapChieu: true,
    },
  ],
  dangChieu: true,
  sapChieu: true,
  arrPhimDefault: [],
  filmDetail: {},
  thongTinPhim: {},
};

export const QuanLyPhimReducers = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_DANH_SACH_PHIM:
      state.arrPhim = payload;
      state.arrPhimDefault = state.arrPhim;
      return { ...state };
    case SET_PHIM_DANG_CHIEU:
      state.dangChieu = !state.dangChieu;
      state.arrPhim = state.arrPhimDefault.filter(
        (film) => film.dangChieu === state.dangChieu
      );
      return { ...state };
    case SET_PHIM_SAP_CHIEU:
      state.sapChieu = !state.sapChieu;
      state.arrPhim = state.arrPhimDefault.filter(
        (film) => film.sapChieu === state.sapChieu
      );
      return { ...state };
    case SET_CHI_TIET_PHIM:
      state.filmDetail = payload;
      return { ...state };
    case SET_THONG_TIN_PHIM:
      state.thongTinPhim = payload;
      return { ...state };

    default:
      return state;
  }
};
