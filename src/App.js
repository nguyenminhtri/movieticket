import { BrowserRouter, Routes, Route } from "react-router-dom";
import Films from "./Pages/Admin/Films/Films";

import CheckOut from "./Pages/CheckOut/CheckOut";

import Detail from "./Pages/Detail/Detail";
import Home from "./Pages/Home/Home";
import Login from "./Pages/Login/Login";

import Register from "./Pages/Register/Register";
import AdminTemplate from "./Templates/AdminTemplate/AdminTemplate";
import { CheckOutTemplate } from "./Templates/CheckOutTemplate/CheckOutTemplate";
import { HomeTemplate } from "./Templates/HomeTemplate/HomeTemplate";
import { UserTemplate } from "./Templates/UserTemplate/UserTemplate";
import Dashboard from "./Pages/Admin/Dashboard/Dashboard";
import ShowTime from "./Pages/Admin/ShowTime/ShowTime";
import Addnew from "./Pages/Admin/Films/Addnew/Addnew";
import Edit from "./Pages/Admin/Films/Edit/Edit";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route
            path="/home"
            element={
              <HomeTemplate>
                <Home />
              </HomeTemplate>
            }
          />
          <Route
            path="/admin"
            element={
              <AdminTemplate>
                <Dashboard />
              </AdminTemplate>
            }
          />
          <Route
            path="/admin/films"
            element={
              <AdminTemplate>
                <Films />
              </AdminTemplate>
            }
          />
          <Route
            path="/admin/films/addnew"
            element={
              <AdminTemplate>
                <Addnew />
              </AdminTemplate>
            }
          />
          <Route
            path="/admin/films/edit/:id"
            element={
              <AdminTemplate>
                <Edit />
              </AdminTemplate>
            }
          />
          <Route
            path="/admin/films/showtime/:id"
            element={
              <AdminTemplate>
                <ShowTime />
              </AdminTemplate>
            }
          />
          <Route
            path="/admin/user"
            element={
              <AdminTemplate>
                <Dashboard />
              </AdminTemplate>
            }
          />
          <Route
            path="/detail/:id"
            element={
              <HomeTemplate>
                <Detail />
              </HomeTemplate>
            }
          />
          <Route
            path="/checkout/:id"
            element={
              <HomeTemplate>
                <CheckOut />
              </HomeTemplate>
            }
          />
          <Route
            path="/login"
            element={
              <UserTemplate>
                <Login />
              </UserTemplate>
            }
          />
          <Route
            path="/register"
            element={
              <UserTemplate>
                <Register />
              </UserTemplate>
            }
          />
          <Route
            path="/"
            element={
              <HomeTemplate>
                <Home />
              </HomeTemplate>
            }
          />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
