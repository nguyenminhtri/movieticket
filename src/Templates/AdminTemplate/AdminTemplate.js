import { FileOutlined, TeamOutlined, UserOutlined } from "@ant-design/icons";
import { Breadcrumb, Layout, Menu, message, theme } from "antd";
import { useState } from "react";
import { useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import Headers from "..//../Templates/HomeTemplate/Layout/Header/Headers";

const { Header, Content, Footer, Sider } = Layout;

function getItem(label, key, icon, children) {
  return {
    key,
    icon,
    children,
    label,
  };
}
const items = [
  getItem(<NavLink to="/admin">User</NavLink>, "/admin", <UserOutlined />),
  getItem(
    <NavLink to="/admin/films">Films</NavLink>,
    "/admin/films",
    <TeamOutlined />,

    [
      getItem(<NavLink to="/admin/films">Film</NavLink>, "3"),
      getItem(<NavLink to="/admin/films/addnew">Add new</NavLink>, "4"),
    ]
  ),
  getItem(
    <NavLink to="/admin/showtime">ShowTime</NavLink>,
    "/admin/showtime",
    <FileOutlined />
  ),
];
export default function AdminTemplate({ children }) {
  const { userLogin } = useSelector((state) => state.QuanLyNguoiDungReducer);
  const navigate = useNavigate();
  if (userLogin !== null && userLogin?.maLoaiNguoiDung !== "QuanTri") {
    message.error("Bạn không đủ quyền truy cập, vui lòng thử lại!!!");
    setTimeout(() => {
      navigate(-1);
    }, 800);
  } else {
    if (userLogin === null) {
      setTimeout(() => {
        navigate("/login");
      }, 800);
      message.error("Vui lòng đăng nhập");
    }
  }
  window.scrollTo(0, 0);
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <div>
      <Headers />
      <Layout
        style={{
          minHeight: "100vh",
        }}
      >
        <Sider
          collapsible
          collapsed={collapsed}
          onCollapse={(value) => {
            console.log(value);

            return setCollapsed(value);
          }}
        >
          <Menu
            theme="dark"
            defaultSelectedKeys={["1"]}
            mode="inline"
            items={items}
          ></Menu>
        </Sider>
        <Layout className="site-layout">
          <Header
            style={{
              padding: 0,
              background: colorBgContainer,
            }}
          />
          <Content
            style={{
              margin: "0 16px",
            }}
          >
            <Breadcrumb
              style={{
                margin: "16px 0",
              }}
            >
              <Breadcrumb.Item>User</Breadcrumb.Item>
              <Breadcrumb.Item>Bill</Breadcrumb.Item>
            </Breadcrumb>
            <div
              style={{
                padding: 24,
                minHeight: 360,
                background: colorBgContainer,
              }}
            >
              {children}
            </div>
          </Content>
          <Footer
            style={{
              textAlign: "center",
            }}
          >
            Ant Design ©2023 Created by Ant UED
          </Footer>
        </Layout>
      </Layout>
    </div>
  );
}
