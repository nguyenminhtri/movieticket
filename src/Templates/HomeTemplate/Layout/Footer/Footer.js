/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { useSelector } from "react-redux";
import _ from "lodash";
export default function Footer(props) {
  const { heThongRapChieu } = useSelector((state) => state.QuanLyRapReducer);
  const arrHeThongRapChieu = _.map(heThongRapChieu, (heThongRap) =>
    _.pick(heThongRap, ["maHeThongRap", "tenHeThongRap", "logo"])
  );

  return (
    <footer className="py-6 dark:bg-gray-800 dark:text-gray-50">
      <div className="container px-6 mx-auto space-y-6 divide-y divide-gray-400 md:space-y-12 divide-opacity-50">
        <div className="grid grid-cols-12">
          <div className="pb-6 col-span-full md:pb-0 md:col-span-6">
            <a
              rel="noopener noreferrer"
              href="#"
              className="flex justify-center space-x-3 md:justify-start"
            >
              <img src="https://i.imgur.com/lC22izJ.png" alt="" />
            </a>
          </div>
          <div className="col-span-6 md:text-left md:col-span-3 p-5 text-center">
            <p className="pb-1 text-lg font-medium ">Đối tác</p>
            <div className="grid grid-cols-3 gap-2 ">
              {arrHeThongRapChieu.map((htr, index) => {
                return (
                  <div key={index}>
                    <img
                      src={htr.logo}
                      className="hover:dark:text-violet-400"
                      style={{ width: "50px" }}
                    />
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <div className="grid justify-center pt-6 lg:justify-between">
          <div className="flex flex-col self-center text-sm text-center md:block lg:col-start-1 md:space-x-6">
            <span>©2022 All rights reserved</span>
          </div>
        </div>
      </div>
    </footer>
  );
}
