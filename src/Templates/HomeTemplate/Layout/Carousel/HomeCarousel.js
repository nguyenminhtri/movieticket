import React, { useEffect } from "react";
import { Carousel } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { getCarouselAction } from "../../../../redux/actions/CarouselAction";
const contentStyle = {
  height: "600",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  backgroundSize: "100%",
  backgroundRepeat: "no-repeat",
};
export default function HomeCarousel() {
  const { arrImg } = useSelector((state) => state.CarouselReducers);
  // console.log("arrImg: ", arrImg);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCarouselAction());
  }, []);

  const renderCarousel = () => {
    return arrImg.map((img, index) => {
      return (
        <div key={index}>
          <div
            style={{
              ...contentStyle,
              backgroundImage: `url(${img.hinhAnh})`,
              maxHeight: "700px",
            }}
          >
            <img
              src={img.hinhAnh}
              alt=""
              className="w-full h-full opacity-0  "
            />
          </div>
        </div>
      );
    });
  };

  return (
    <div>
      <Carousel effect="fade">{renderCarousel()}</Carousel>
    </div>
  );
}
