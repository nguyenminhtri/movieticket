import { Fragment } from "react";
import { Popover, Transition } from "@headlessui/react";
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
import { USER_LOGIN } from "../../../../redux/types/Type";

export default function Headers() {
  const { userLogin } = useSelector((state) => state.QuanLyNguoiDungReducer);

  return (
    <Popover className="relative bg-gray-800">
      <div className="mx-auto max-w-7xl px-6">
        <div className="flex items-center justify-between border-b-2 border-gray-100 py-6 md:justify-start md:space-x-10">
          <div className="flex justify-start lg:w-0 lg:flex-1">
            <NavLink to="/">
              <img
                className="h-8 w-auto sm:h-10"
                src="https://i.imgur.com/lC22izJ.png"
                alt=""
              />
            </NavLink>
          </div>
          <div className="-my-2 -mr-2 md:hidden">
            <Popover.Button className="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
              <span className="sr-only">Open menu</span>
              <Bars3Icon className="h-6 w-6" aria-hidden="true" />
            </Popover.Button>
          </div>
          <Popover.Group as="nav" className="hidden space-x-10 md:flex">
            <NavLink
              to="/home"
              className="text-base font-medium text-gray-500 hover:text-gray-900"
            >
              Phim
            </NavLink>
            <NavLink
              to="/admin"
              className="text-base font-medium text-gray-500 hover:text-gray-900"
            >
              Admin
            </NavLink>
          </Popover.Group>
          <div className="hidden items-center justify-end md:flex md:flex-1 lg:w-0">
            {userLogin === null ? (
              <>
                <NavLink
                  to="/login"
                  className="whitespace-nowrap text-base font-medium text-gray-500 hover:text-gray-900"
                >
                  Đăng nhập
                </NavLink>
                <NavLink
                  to="/register"
                  className="ml-8 inline-flex items-center justify-center whitespace-nowrap rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700"
                >
                  Đăng kí
                </NavLink>
              </>
            ) : (
              <>
                <NavLink
                  to="/login"
                  className="whitespace-nowrap text-base font-medium text-gray-500 "
                >
                  hi, {userLogin.taiKhoan}
                </NavLink>
                <button
                  onClick={() => {
                    localStorage.removeItem(USER_LOGIN);
                    window.location.href = "/";
                  }}
                  className="ml-8 inline-flex items-center justify-center whitespace-nowrap rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700"
                >
                  Đăng xuất
                </button>
              </>
            )}
          </div>
        </div>
      </div>

      <Transition
        as={Fragment}
        enter="duration-200 ease-out"
        enterFrom="opacity-0 scale-95"
        enterTo="opacity-100 scale-100"
        leave="duration-100 ease-in"
        leaveFrom="opacity-100 scale-100"
        leaveTo="opacity-0 scale-95"
      >
        <Popover.Panel
          focus
          className="absolute inset-x-0 top-0 origin-top-right transform p-2 transition md:hidden z-20"
        >
          <div className="divide-y-2 divide-gray-50 rounded-lg bg-white shadow-lg ring-1 ring-black ring-opacity-5">
            <div className="px-5 pt-5 pb-6">
              <div className="flex items-center justify-between">
                <div>
                  <img
                    className="h-8 w-auto"
                    src="https://i.imgur.com/lC22izJ.png"
                    alt="Your Company"
                  />
                </div>
                <div className="-mr-2">
                  <Popover.Button className="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                    <span className="sr-only">Close menu</span>
                    <XMarkIcon className="h-6 w-6" aria-hidden="true" />
                  </Popover.Button>
                </div>
              </div>
              <div className="mt-6"></div>
            </div>
            <div className="space-y-6 py-6 px-5">
              <div className="grid grid-cols-2 gap-y-4 gap-x-8">
                <NavLink
                  to="/"
                  className="text-base font-medium text-gray-900 hover:text-gray-700"
                >
                  Phim
                </NavLink>

                <NavLink
                  to="/admin"
                  className="text-base font-medium text-gray-900 hover:text-gray-700"
                >
                  Admin
                </NavLink>
              </div>
              {userLogin === null ? (
                <div>
                  <NavLink
                    to="/login"
                    className="flex w-full items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700"
                  >
                    Đăng Nhập
                  </NavLink>
                  <p className="mt-6 text-center text-base font-medium text-gray-500">
                    Bạn chưa có tài khoản ?
                    <NavLink
                      to="/register"
                      className="text-indigo-600 hover:text-indigo-500"
                    >
                      Đăng kí
                    </NavLink>
                  </p>
                </div>
              ) : (
                <div>
                  <NavLink
                    to="/login"
                    className="flex w-full items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700"
                  >
                    {userLogin.taiKhoan}
                  </NavLink>
                  <p className="mt-6 text-center text-base font-medium text-gray-500">
                    <button
                      onClick={() => {
                        localStorage.removeItem(USER_LOGIN);
                        window.location.href = "/";
                      }}
                      className="text-indigo-600 hover:text-indigo-500"
                    >
                      Đăng xuất
                    </button>
                  </p>
                </div>
              )}
            </div>
          </div>
        </Popover.Panel>
      </Transition>
    </Popover>
  );
}
