import Footer from "./Layout/Footer/Footer";
import Header from "./Layout/Header/Headers";

export const HomeTemplate = ({ children }) => {
  return (
    <div>
      <Header />
      {children}
      <Footer />
    </div>
  );
};
