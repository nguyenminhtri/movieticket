import Lottie from "lottie-react";
import bg_animate from "../../assets/133558-happy-new-year-2023.json";
import Footer from "../HomeTemplate/Layout/Footer/Footer";
import Header from "../HomeTemplate/Layout/Header/Headers";
/* eslint-disable jsx-a11y/img-redundant-alt */
export const UserTemplate = ({ children }) => {
  return (
    <section className="h-full bg-yellow-300">
      <Header />
      <div className="px-6 h-full text-gray-800">
        <div className="flex xl:justify-center lg:justify-between justify-center items-center flex-wrap h-full g-6">
          <div className="grow-0 shrink-1 md:shrink-0 basis-auto xl:w-6/12 lg:w-6/12 md:w-9/12 mb-12 md:mb-0">
            <Lottie animationData={bg_animate} loop={true} />
          </div>
          {children}
        </div>
      </div>
      <Footer />
    </section>
  );
};
