/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { USER_LOGIN } from "../../redux/types/Type";

export default function HeaderCheckOut(props) {
  const { userLogin } = useSelector((state) => state.QuanLyNguoiDungReducer);

  const navigate = useNavigate();
  useEffect(() => {
    if (!userLogin || userLogin.maLoaiNguoiDung !== "QuanTri") {
      navigate("/");
    }
  });
  const renderContent = () => {
    if (userLogin) {
      return (
        <div>
          <NavLink to="/login" className="mr-5">
            hi, {userLogin.hoTen}
          </NavLink>
          <button
            onClick={() => {
              localStorage.removeItem(USER_LOGIN);
              window.location.href = "/";
            }}
          >
            Đăng xuất
          </button>
        </div>
      );
    } else {
      return (
        <div className="items-center flex-shrink-0 hidden lg:flex">
          <NavLink to="/login" className="self-center px-8 py-3 rounded">
            Đăng nhập
          </NavLink>
        </div>
      );
    }
  };
  return (
    <div className=" dark:text-black bg-slate-400 z-20 w-full ">
      <div className="container flex justify-evenly h-16 items-center">
        <NavLink
          to="/"
          aria-label="Back to homepage"
          className="flex items-center p-2 text-3xl text-green-600  "
        >
          Cinema
        </NavLink>
        <ul className="items-stretch hidden space-x-3 lg:flex">
          <li>
            <input
              type="text"
              id="first_name"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500 outline-none"
              placeholder="Phim..."
              required
            />
          </li>
          <li>{renderContent()}</li>
        </ul>

        <button className="p-4 lg:hidden">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            className="w-6 h-6 dark:text-gray-100"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M4 6h16M4 12h16M4 18h16"
            ></path>
          </svg>
        </button>
      </div>
    </div>
  );
}
