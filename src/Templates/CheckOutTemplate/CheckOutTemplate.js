import Footer from "../HomeTemplate/Layout/Footer/Footer";
import Headers from "../HomeTemplate/Layout/Header/Headers";

export const CheckOutTemplate = ({ children }) => {
  return (
    <div>
      <Headers />
      {children}
      <Footer />
    </div>
  );
};
