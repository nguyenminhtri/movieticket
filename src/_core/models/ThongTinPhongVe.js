export class ThongTinLichChieu {
  thongTinPhim = new ThongTinPhim();
  danhSachGhe = [];
}
class ThongTinPhim {
  maLichChieu = "";
  tenCumRap = "";
  tenRap = "";
  diaChi = "";
  tenPhim = "";
  hinhAnh = "";
  ngayChieu = "";
  gioChieu = "";
}
class DanhSachGhe {
  maGhe = "";
  tenGhe = "";
  maRap = "";
  loaiGhe = "";
  stt = "";
  giaVe = "";
  daDat = "";
  taiKhoanNguoiDat = "";
}
