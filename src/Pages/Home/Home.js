/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import MultipleRows from "../../Components/RSlick/MultipleRows";
import { layDanhSachPhimAction } from "../../redux/actions/QuanLyPhimAction";
import { layDanhSachRapAction } from "../../redux/actions/QuanLyRapAction";

import HomeCarousel from "../../Templates/HomeTemplate/Layout/Carousel/HomeCarousel";
import HomeMenu from "./HomeMenu/HomeMenu";

export default function Home() {
  const { arrPhim } = useSelector((state) => state.QuanLyPhimReducers);
  const { heThongRapChieu } = useSelector((state) => state.QuanLyRapReducer);
  const dispatch = useDispatch();
  useEffect(() => {
    function fetchData() {
      dispatch(layDanhSachPhimAction());
      dispatch(layDanhSachRapAction());
    }
    fetchData();
  }, []);
  return (
    <div>
      <HomeCarousel />
      <section className="text-white bg-gray-800">
        <div className="container px-5 pt-12 mx-auto w-full ">
          <MultipleRows arrPhim={arrPhim} />
        </div>
        <div className="max-sm:hidden ">
          <div className=" text-center text-5xl py-5 text-green-500">
            Cụm rạp
          </div>
          <div className="rounded w-3/4 mx-auto bg-gray-900 border-solid border-white">
            <HomeMenu heThongRapChieu={heThongRapChieu} />
          </div>
        </div>
      </section>
    </div>
  );
}
