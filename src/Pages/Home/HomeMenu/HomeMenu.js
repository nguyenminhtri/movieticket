import React from "react";
import { Tabs } from "antd";
import MovieTabItem from "./MovieTabItem";
const onChange = (key) => {};

export default function HomeMenu(props) {
  const { heThongRapChieu } = props;
  console.log("heThongRapChieu: ", heThongRapChieu);
  const renderHeThongRap = () => {
    return heThongRapChieu.map((heThongRap, index) => {
      return {
        label: (
          <img
            key={index}
            src={heThongRap.logo}
            alt=""
            className="w-16 h-16 object-cover"
          />
        ),
        key: heThongRap.maHeThongRap,
        children: (
          <Tabs
            style={{
              height: "600px",
              width: "1000px",
            }}
            type="card"
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={heThongRap.lstCumRap.map((rap, index) => {
              return {
                label: <div className="text-green-500">{rap.tenCumRap}</div>,
                key: rap.maCumRap,
                children: (
                  <div
                    style={{
                      overflowY: "scroll",
                      height: "600px",
                    }}
                  >
                    {rap.danhSachPhim.map((phim, index) => {
                      return <MovieTabItem key={index} movie={phim} />;
                    })}
                  </div>
                ),
              };
            })}
          />
        ),
      };
    });
  };

  return (
    <Tabs
      type="card"
      style={{ height: "600px" }}
      tabPosition="left"
      defaultActiveKey="1"
      onChange={onChange}
      items={renderHeThongRap()}
    />
  );
}
