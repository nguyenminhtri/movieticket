import moment from "moment/moment";
import React from "react";
import { NavLink } from "react-router-dom";

export default function MovieTabItem({ movie }) {
  return (
    <div className="flex p-3 text-left ">
      <img
        className="w-24 h-40 object-cover mr-5 rounded"
        src={movie.hinhAnh}
        alt=""
      />
      <div>
        <h5 className="font-medium mb-5 text-white">{movie.tenPhim}</h5>
        <div className="grid grid-cols-3 gap-4">
          {movie.lstLichChieuTheoPhim.slice(0, 9).map((item, index) => {
            return (
              <NavLink
                key={index}
                to={`/checkout/${item.maLichChieu}`}
                className="bg-red-500 text-white rounded p-2"
              >
                {moment(item.ngayChieuGioChieu).format("DD/MM/YYYY - hh:mm A")}
              </NavLink>
            );
          })}
        </div>
      </div>
    </div>
  );
}
