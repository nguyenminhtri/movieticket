/* eslint-disable no-unreachable */
/* eslint-disable import/no-anonymous-default-export */
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  datVeAction,
  layChiTietPhongVeAction,
} from "../../redux/actions/QuanLyDatVeAction";
import style from "./Checkout.module.css";
import "./CheckOut.css";
import { DAT_VE, RETURN_TAB, USER_LOGIN } from "../../redux/types/Type";
import _ from "lodash";
import { ThongTinDatVe } from "../../_core/models/ThongTinDatVe";
import { message, Tabs } from "antd";
import { layThongTinNguoiDungAction } from "../../redux/actions/QuanLyNguoiDungAction";
import moment from "moment/moment";
import { useNavigate, useParams } from "react-router-dom";
function CheckOut() {
  const navigate = useNavigate();

  let { id } = useParams();
  let dispatch = useDispatch();
  let { userLogin } = useSelector((state) => state.QuanLyNguoiDungReducer);
  if (userLogin === null) {
    window.location.href = "/login";
    message.error("Vui lòng đăng nhập");
  }

  let { chiTietPhongVe, danhSachGheDangDat, danhSachGheKhachDat } = useSelector(
    (state) => state.QuanLyDatVeReducer
  );

  let { danhSachGhe, thongTinPhim } = chiTietPhongVe;

  useEffect(() => {
    dispatch(layChiTietPhongVeAction(id));
  }, []);
  const renderSeats = () => {
    return danhSachGhe?.map((item, index) => {
      let classGheVip = item.loaiGhe === "Vip" ? "gheVip" : "";
      let classDaDat = item.daDat ? "gheDaDat" : "";
      let classGheDangDat = "";
      let classGheKhachDat = "";
      let indexGheKhachDat = danhSachGheKhachDat.findIndex(
        (gheKhachDat, index) => {
          return gheKhachDat.maGhe === item.maGhe;
        }
      );
      if (indexGheKhachDat !== -1) {
        classGheKhachDat = "gheKhachDat";
      }
      let indexGheDD = danhSachGheDangDat.findIndex((gheDD) => {
        return gheDD.maGhe === item.maGhe;
      });
      let classGheMinhDat = "";
      if (userLogin.taiKhoan === item.taiKhoanNguoiDat) {
        classGheMinhDat = "gheMinhDat";
      }
      if (indexGheDD !== -1) {
        classGheDangDat = "gheDangDat";
      }

      return (
        <>
          <button
            onClick={() => {
              dispatch({
                type: DAT_VE,
                payload: item,
              });
            }}
            key={index}
            disabled={item.daDat || classGheKhachDat !== ""}
            className={`ghe ${classGheVip} ${classDaDat} ${classGheDangDat} ${classGheMinhDat} ${classGheKhachDat}`}
          >
            {item.stt}
          </button>
        </>
      );
    });
  };
  return (
    <div className="container text-left min-h-screen mt-5">
      <div className="grid grid-cols-3">
        <div className="col-span-2">
          <div className={`${style.trapezoid}`}></div>
          <div>{renderSeats()}</div>
          <div className="flex flex-row">
            <div className="flex flex-row items-center">
              <div className="ghe"></div>
              <div> Ghế chưa đặt</div>
            </div>
            <div className="flex flex-row items-center">
              <div className="ghe gheDangDat"></div>
              <div> Ghế đang chọn</div>
            </div>
            <div className="flex flex-row items-center">
              <div className="ghe gheVip"></div>
              <div> Ghế vip</div>
            </div>
            <div className="flex flex-row items-center">
              <div className="ghe gheMinhDat"></div>
              <div> Ghế đã đặt</div>
            </div>
          </div>
        </div>
        <div className="col-span-1">
          <h2 className="text-green-400 text-4xl">
            {danhSachGheDangDat
              .reduce((tt, item, index) => {
                return (tt += item.giaVe);
              }, 0)
              .toLocaleString()}
            đ
          </h2>
          <hr className="my-3" />
          <h1>{thongTinPhim.tenPhim}</h1>
          <p>
            Địa điểm: {thongTinPhim.tenCumRap} - {thongTinPhim.tenRap}
          </p>
          <p>
            Ngày chiếu: {thongTinPhim.ngayChieu} - {thongTinPhim.gioChieu}
          </p>
          <hr className="my-3" />
          <div className="flex flex-row">
            <div className="w-4/5">
              <span>Ghế</span>
              {_.sortBy(danhSachGheDangDat, ["stt"]).map((geDD, index) => {
                return (
                  <span key={index} className="text-green-400 mx-2">
                    {geDD.stt}
                  </span>
                );
              })}
            </div>
            <div className="text-right col-span-1">
              <span>
                {danhSachGheDangDat
                  .reduce((tt, item, index) => {
                    return (tt += item.giaVe);
                  }, 0)
                  .toLocaleString()}
                đ
              </span>
            </div>
          </div>
          <hr className="my-3" />
          <div>
            <i>Email</i>
            <p>{userLogin.email}</p>
          </div>
          <hr className="my-3" />
          <div>
            <i>Phone</i>
            <p>{userLogin.soDT}</p>
          </div>
          <div className=" h-full mt-5 items-center">
            <button
              onClick={() => {
                const thongTinDatVe = new ThongTinDatVe();
                thongTinDatVe.maLichChieu = id;
                thongTinDatVe.danhSachVe = danhSachGheDangDat;

                dispatch(datVeAction(thongTinDatVe));
                if (!localStorage.getItem(USER_LOGIN)) {
                  navigate("/login");
                }
              }}
              className="py-2 px-4 bg-green-400 rounded w-full"
            >
              Đặt vé
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
function KetQua(props) {
  const dispatch = useDispatch();
  const { thongTinNguoiDung, userLogin } = useSelector(
    (state) => state.QuanLyNguoiDungReducer
  );

  useEffect(() => {
    dispatch(layThongTinNguoiDungAction(userLogin));
  }, [thongTinNguoiDung]);

  const renderStiket = () => {
    return thongTinNguoiDung.thongTinDatVe?.map((item, index) => {
      return (
        <div className="p-2 lg:w-1/3 md:w-1/2 w-full" key={index}>
          <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
            <img
              alt="team"
              className="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4"
              src={item.hinhAnh}
              style={{ width: "80px", height: "80px" }}
            />
            <div className="flex-grow">
              <h2 className="text-gray-900 title-font font-medium">
                {item.tenPhim}
              </h2>
              <p className="text-gray-500"> Giá vé: {item.giaVe}</p>
              <p>{moment(item.ngayDat).format("DD/MM/YYYY hh:mm A")}</p>
              <p>
                Địa điểm: {_.first(item.danhSachGhe).tenHeThongRap} -
                {_.first(item.danhSachGhe).tenCumRap}
              </p>
              <p>
                Ghế:
                {item.danhSachGhe.map((ghe, index) => {
                  return <span key={index}> {ghe.tenGhe}, </span>;
                })}
              </p>
            </div>
          </div>
        </div>
      );
    });
  };
  return (
    <section className="text-gray-600 body-font">
      <div className="container px-5 py-24 mx-auto">
        <div className="flex flex-col text-center w-full mb-20">
          <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-blue-900">
            Lịch sử đặt vé
          </h1>
          <p className="lg:w-2/3 mx-auto leading-relaxed text-base">
            Chúc bạn xem phim vui vẻ!
          </p>
        </div>
        <div className="flex flex-wrap -m-2">{renderStiket()}</div>
      </div>
    </section>
  );
}

export default (props) => {
  const dispatch = useDispatch();

  const { tabActive } = useSelector((state) => state.QuanLyDatVeReducer);

  return (
    <div className="">
      <Tabs
        defaultActiveKey="1"
        activeKey={tabActive}
        items={[
          {
            key: "1",
            label: `Chọn ghé thanh toán`,
            children: <CheckOut {...props} />,
          },
          {
            key: "2",
            label: `Kết quả đặt vé`,
            children: <KetQua {...props} />,
          },
        ]}
        onChange={(key) => {
          dispatch({ type: RETURN_TAB, payload: key });
        }}
      />
    </div>
  );
};
