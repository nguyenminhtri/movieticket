import { Progress, Space } from "antd";
export default function CircleRating({ danhgia }) {
  console.log(danhgia);

  return (
    <Space wrap>
      <Progress type="circle" percent={`${danhgia * 10}`} />
    </Space>
  );
}
