import React, { useEffect } from "react";

import { Tabs } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { layThongTinChiTietPhim } from "../../redux/actions/QuanLyRapAction";
import moment from "moment/moment";

import { NavLink, useParams } from "react-router-dom";
import { Rate } from "antd";

const desc = ["terrible", "bad", "normal", "good", "wonderful"];
const onChange = (key) => {};

export default function Detail({ children }) {
  const { filmDetail } = useSelector((state) => state.QuanLyPhimReducers);
  console.log("filmDetail: ", filmDetail);
  let { id } = useParams();
  const dispatch = useDispatch();
  useEffect(() => {
    window.scrollTo(0, 0);
    dispatch(layThongTinChiTietPhim(id));
  }, []);
  const renderHeThongRap = () => {
    return filmDetail.heThongRapChieu?.map((heThongRap, index) => {
      return {
        label: (
          <img
            src={heThongRap.logo}
            alt=""
            className="w-16 h-16 object-cover"
          />
        ),
        key: heThongRap.maHeThongRap,
        children: (
          <Tabs
            className="text-white p-4"
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={heThongRap.cumRapChieu?.map((rap, index) => {
              return {
                label: (
                  <div className="">
                    <div> {rap.tenCumRap}</div>
                    <div>
                      Địa chỉ:
                      {rap.diaChi.length > 40
                        ? rap.diaChi.slice(0, 40) + "..."
                        : rap.diaChi}
                    </div>
                  </div>
                ),
                key: rap.maCumRap,
                children: rap.lichChieuPhim.map((lichchieu, index) => {
                  return (
                    <NavLink
                      key={index}
                      to={`/checkout/${lichchieu.maLichChieu}`}
                      className="p-4"
                    >
                      {moment(lichchieu.ngayChieuGioChieu).format("hh:mm A")}
                    </NavLink>
                  );
                }),
              };
            })}
          />
        ),
      };
    });
  };
  return (
    <div className=" w-full h-screen bg-gray-700">
      <div className="grid grid-cols-12">
        <div className="col-span-4 col-start-4 flex flex-row py-5">
          <img
            className="w-60 h-60 rounded-2xl "
            src={filmDetail.hinhAnh}
            alt=""
          />

          <div className="px-5">
            <h2 className="ml-6 text-violet-500 text-2xl font-bold">
              {filmDetail.tenPhim}
            </h2>

            <p className="text-white font-thin">{filmDetail.moTa}</p>
          </div>
        </div>
        <div className="col-span-2 col-start-9 text-center">
          <p className="text-violet-500 text-2xl  my-5">Đánh giá</p>
          <div>
            <Rate tooltips={desc} value={filmDetail.danhGia / 2} />
          </div>
        </div>
      </div>
      <div className="bg-gray-800 h-64 w-3/4  mx-auto text-white rounded">
        <Tabs
          type="card"
          tabPosition="left"
          defaultActiveKey="1"
          onChange={onChange}
          items={renderHeThongRap()}
        />
      </div>
    </div>
  );
}
