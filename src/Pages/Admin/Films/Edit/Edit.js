import { DatePicker, Form, Input, InputNumber, Switch } from "antd";
import dayjs from "dayjs";
import { useFormik } from "formik";
import moment from "moment/moment";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import {
  CapNhatPhimUploadAction,
  layThongTinPhimAction,
} from "../../../../redux/actions/QuanLyPhimAction";
export default function Edit() {
  const [componentSize, setComponentSize] = useState("default");
  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };
  const dispatch = useDispatch();
  const [img, setImg] = useState("");
  let { id } = useParams();
  const { thongTinPhim } = useSelector((state) => state.QuanLyPhimReducers);
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(layThongTinPhimAction(id));
  }, []);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      maPhim: thongTinPhim.maPhim,
      tenPhim: thongTinPhim.tenPhim,
      trailer: thongTinPhim.trailer,
      moTa: thongTinPhim.moTa,
      ngayKhoiChieu: moment(thongTinPhim.ngayKhoiChieu).format("DD/MM/YYYY"),
      dangChieu: thongTinPhim.dangChieu,
      sapChieu: thongTinPhim.sapChieu,
      hot: thongTinPhim.hot,
      danhGia: thongTinPhim.danhGia,
      hinhAnh: null,
      maNhom: thongTinPhim.maNhom,
    },
    onSubmit: (values) => {
      console.log("values: ", values);
      //tạo đối tượng formData
      let formData = new FormData();
      for (let key in values) {
        if (key !== "hinhAnh") {
          formData.append(key, values[key]);
        } else {
          if (values.hinhAnh !== null) {
            formData.append("File", values.hinhAnh, values.hinhAnh.name);
          }
        }
      }
      dispatch(CapNhatPhimUploadAction(formData, navigate));
    },
  });

  const onchangeDatePicket = (value) => {
    formik.setFieldValue(
      "ngayKhoiChieu",
      moment(value.toJSON()).format("DD/MM/YYYY")
    );
  };
  const onchangeField = (name) => {
    return (value) => formik.setFieldValue(name, value);
  };
  const handleChangeFile = async (e) => {
    let file = e.target.files[0];
    if (
      file.type === "image/jpeg" ||
      file.type === "image/jpg" ||
      file.type === "image/png" ||
      file.type === "image/gif"
    ) {
      await formik.setFieldValue("hinhAnh", file);
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e) => {
        console.log(e.target.result);
        setImg(e.target.result);
      };
    }
  };

  return (
    <>
      <div className="m-5 text-blue-500 text-2xl">Cập nhật phim</div>
      <Form
        onSubmitCapture={formik.handleSubmit}
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        initialValues={{
          size: componentSize,
        }}
        onValuesChange={onFormLayoutChange}
        size={componentSize}
        style={{
          maxWidth: 600,
        }}
      >
        <Form.Item label="Tên phim">
          <Input
            name="tenPhim"
            onChange={formik.handleChange}
            value={formik.values.tenPhim}
          />
        </Form.Item>
        <Form.Item label="Trailer">
          <Input
            name="trailer"
            onChange={formik.handleChange}
            value={formik.values.trailer}
          />
        </Form.Item>
        <Form.Item label="Mô tả">
          <Input
            name="moTa"
            onChange={formik.handleChange}
            value={formik.values.moTa}
          />
        </Form.Item>
        <Form.Item label="Mã nhóm">
          <Input
            name="maNhom"
            onChange={formik.handleChange}
            value={formik.values.maNhom}
          />
        </Form.Item>
        <Form.Item label="Ngày khởi chiếu">
          <DatePicker
            format={"DD/MM/YYYY"}
            name="ngayKhoiChieu"
            onChange={onchangeDatePicket}
            value={dayjs(formik.values.ngayKhoiChieu, "DD/MM/YYYY")}
          />
        </Form.Item>
        <Form.Item label="Đang chiếu" valuePropName="checked">
          <Switch
            onChange={onchangeField("dangChieu")}
            checked={formik.values.dangChieu}
          />
        </Form.Item>
        <Form.Item label="Sắp chiếu" valuePropName="checked">
          <Switch
            onChange={onchangeField("sapChieu")}
            checked={formik.values.sapChieu}
          />
        </Form.Item>
        <Form.Item label="Hot" valuePropName="checked">
          <Switch onChange={onchangeField("hot")} checked={formik.values.hot} />
        </Form.Item>
        <Form.Item label="Đánh giá">
          <InputNumber
            onChange={onchangeField("danhGia")}
            min={1}
            max={10}
            value={formik.values.danhGia}
          />
        </Form.Item>
        <Form.Item label="File">
          <Input
            type="file"
            onChange={handleChangeFile}
            accept="image/png, image/jpeg, image/gif,image/jpg"
          />
          <img
            src={img === "" ? thongTinPhim.hinhAnh : img}
            style={{ width: "100px", height: "100px" }}
            alt="... "
          />
        </Form.Item>
        <Form.Item>
          <button
            type="submit"
            className="inline-flex justify-center rounded-lg text-sm font-semibold py-3 px-4 bg-slate-900 text-white hover:bg-slate-700"
          >
            Cập nhật
          </button>
        </Form.Item>
      </Form>
    </>
  );
}
