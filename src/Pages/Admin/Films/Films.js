import { Table } from "antd";
import {
  CalendarOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { Input } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import {
  layDanhSachPhimAction,
  xoaPhimAction,
} from "../../../redux/actions/QuanLyPhimAction";

import { NavLink } from "react-router-dom";
const { Search } = Input;

export default function Films() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(layDanhSachPhimAction());
  }, []);

  const { arrPhimDefault } = useSelector((state) => state.QuanLyPhimReducers);

  const onSearch = (value) => {
    dispatch(layDanhSachPhimAction(value));
  };
  const columns = [
    {
      title: "Mã phim",
      dataIndex: "maPhim",
      width: 100,
      // specify the condition of filtering result
      // here is that finding the name started with `value`
      onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.maPhim - b.maPhim,
      sortDirections: ["ascend"],
      sortOrder: ["descend"],
    },
    {
      title: "Tên Phim",
      dataIndex: "tenPhim",
      defaultSortOrder: "descend",
      sorter: (a, b) =>
        a.tenPhim.toLowerCase().trim() - b.tenPhim.toLowerCase().trim(),
      width: 300,
    },

    {
      title: "Hình ảnh",
      dataIndex: "hinhAnh",
      render: (text) => {
        return (
          <img src={text} alt="" style={{ width: "50px", height: "50px" }} />
        );
      },
      width: 100,
    },
    {
      title: "Mô tả",
      dataIndex: "moTa",
      render: (text) => {
        return text.length > 60 ? (
          <span>{text.slice(0, 50) + " ..."}</span>
        ) : (
          <span>{text}</span>
        );
      },
    },
    {
      title: "Hành động",
      dataIndex: "hanhDong",
      render: (text, film) => {
        return (
          <div>
            <NavLink
              to={`/admin/films/edit/${film.maPhim}`}
              key={1}
              className="text-yellow-500 mx-3 text-2xl"
            >
              <EditOutlined />
            </NavLink>
            <button
              key={2}
              onClick={() => {
                if (window.confirm("Bạn có muốn xóa phim " + film.tenPhim)) {
                  dispatch(xoaPhimAction(film.maPhim));
                }
              }}
              className="text-red-500 text-2xl"
            >
              <DeleteOutlined />
            </button>
            <NavLink
              to={`/admin/films/showtime/${film.maPhim}`}
              key={1}
              className="text-blue-500 mx-3 text-2xl"
            >
              <CalendarOutlined />
            </NavLink>
          </div>
        );
      },
    },
  ];

  const data = arrPhimDefault;
  const onChange = (pagination, filters, sorter, extra) => {
    console.log("params", pagination, filters, sorter, extra);
  };

  return (
    <div>
      <NavLink to="/admin/films/addnew">Thêm phim</NavLink>
      <Search
        placeholder="Tên phim..."
        allowClear
        size="large"
        onSearch={onSearch}
        className="my-3"
      />
      <Table
        rowKey={(record) => {
          return record.maPhim;
        }}
        columns={columns}
        dataSource={data}
        onChange={onChange}
      />
      ;
    </div>
  );
}
