import { DatePicker, Form, Input, InputNumber, Switch } from "antd";
import { useFormik } from "formik";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { themPhimUploadHinhAction } from "../../../../redux/actions/QuanLyPhimAction";
export default function Addnew() {
  const [componentSize, setComponentSize] = useState("default");
  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };
  const dispatch = useDispatch();
  const [img, setImg] = useState("");
  const formik = useFormik({
    initialValues: {
      tenPhim: "",
      trailer: "",
      moTa: "",
      ngayKhoiChieu: "",
      dangChieu: false,
      sapChieu: false,
      hot: false,
      danhGia: 0,
      hinhAnh: {},
      maNhom: "",
    },
    onSubmit: (values) => {
      console.log("values: ", values);
      //tạo đối tượng formData
      let formData = new FormData();
      for (let key in values) {
        if (key !== "hinhAnh") {
          formData.append(key, values[key]);
        } else {
          formData.append("File", values.hinhAnh, values.hinhAnh.name);
        }
      }
      dispatch(themPhimUploadHinhAction(formData));
    },
  });
  const onchangeDatePicket = (value, DateString) => {
    formik.setFieldValue("ngayKhoiChieu", DateString);
  };
  const onchangeField = (name) => {
    return (value) => formik.setFieldValue(name, value);
  };
  const handleChangeFile = (e) => {
    let file = e.target.files[0];
    console.log(file);
    if (
      file.type === "image/jpeg" ||
      file.type === "image/jpg" ||
      file.type === "image/png" ||
      file.type === "image/gif"
    ) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e) => {
        console.log(e.target.result);
        setImg(e.target.result);
      };
      formik.setFieldValue("hinhAnh", file);
    }
  };

  return (
    <>
      <div className="m-5 text-blue-500 text-2xl">Thêm phim mới</div>
      <Form
        onSubmitCapture={formik.handleSubmit}
        labelCol={{
          span: 6,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        initialValues={{
          size: componentSize,
        }}
        onValuesChange={onFormLayoutChange}
        size={componentSize}
        style={{
          maxWidth: 600,
        }}
      >
        <Form.Item label="Tên phim">
          <Input name="tenPhim" onChange={formik.handleChange} />
        </Form.Item>
        <Form.Item label="Trailer">
          <Input name="trailer" onChange={formik.handleChange} />
        </Form.Item>
        <Form.Item label="Mô tả">
          <Input name="moTa" onChange={formik.handleChange} />
        </Form.Item>
        <Form.Item label="Mã nhóm">
          <Input name="maNhom" onChange={formik.handleChange} />
        </Form.Item>
        <Form.Item label="Ngày khởi chiếu">
          <DatePicker
            format={"DD/MM/YYYY"}
            name="ngayKhoiChieu"
            onChange={onchangeDatePicket}
          />
        </Form.Item>
        <Form.Item label="Đang chiếu" valuePropName="checked">
          <Switch onChange={onchangeField("dangChieu")} />
        </Form.Item>
        <Form.Item label="Sắp chiếu" valuePropName="checked">
          <Switch onChange={onchangeField("sapChieu")} />
        </Form.Item>
        <Form.Item label="Hot" valuePropName="checked">
          <Switch onChange={onchangeField("hot")} />
        </Form.Item>
        <Form.Item label="Đánh giá">
          <InputNumber onChange={onchangeField("danhGia")} min={1} max={10} />
        </Form.Item>
        <Form.Item label="File">
          <Input
            type="file"
            onChange={handleChangeFile}
            accept="image/png, image/jpeg, image/gif,image/jpg"
          />
          <img
            src={img}
            style={{ width: "100px", height: "100px" }}
            alt="... "
          />
        </Form.Item>
        <Form.Item>
          <button
            type="submit"
            className="inline-flex justify-center rounded-lg text-sm font-semibold py-3 px-4 bg-slate-900 text-white hover:bg-slate-700"
          >
            Thêm phim
          </button>
        </Form.Item>
      </Form>
    </>
  );
}
