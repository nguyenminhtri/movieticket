/* eslint-disable react-hooks/exhaustive-deps */
import { DatePicker, Form, InputNumber, Select } from "antd";
import { useFormik } from "formik";
import moment from "moment/moment";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { quanLyDatVeServices } from "../../../services/QuanLyDatVeService";
import { quanLyRapService } from "../../../services/QuanLyRapService";

export default function ShowTime() {
  const { id } = useParams();

  const formik = useFormik({
    initialValues: {
      maPhim: id,
      ngayChieuGioChieu: "",
      maRap: "",
      giaVe: 75000,
    },
    onSubmit: async (value) => {
      console.log(value);

      try {
        const result = await quanLyDatVeServices.taoLichChieu(value);
        console.log("TPV", result.data.content);
        alert("them lich chieu thanh cong");
      } catch (error) {
        console.log(error);
      }
    },
  });

  const [state, setState] = useState({
    heThongRapChieu: [],
    cumRapChieu: [],
  });

  useEffect(() => {
    async function fetchData() {
      try {
        let result = await quanLyRapService.LayThongTinHeThongRap();
        setState({
          ...state,
          heThongRapChieu: result.data.content,
        });
      } catch (error) {}
    }
    fetchData();
  }, []);

  const [componentSize, setComponentSize] = useState("default");
  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };
  const renderHeThongRapChieu = () => {
    return state.heThongRapChieu?.map((rap, i) => {
      return (
        <Select.Option key={i} value={rap.maHeThongRap}>
          {rap.maHeThongRap}
        </Select.Option>
      );
    });
  };
  const renderCumRapChieu = () => {
    return state.cumRapChieu?.map((rap, i) => {
      return (
        <Select.Option key={i} value={rap.maCumRap}>
          {rap.tenCumRap}
        </Select.Option>
      );
    });
  };
  const handleChange = async (value) => {
    try {
      let result = await quanLyRapService.LayThongTinCumRap(value);
      setState({
        ...state,
        cumRapChieu: result.data.content,
      });
    } catch (error) {}
  };
  const onChange = (value, dateString) => {
    formik.setFieldValue(
      "ngayChieuGioChieu",
      moment(dateString).format("DD/MM/YYYY hh:mm:ss")
    );
  };
  const onchangeField = (name) => {
    return (value) => formik.setFieldValue(name, value);
  };
  return (
    <>
      <div className="m-5 text-blue-500 text-2xl">Tạo lịch chiếu</div>
      <Form
        onSubmitCapture={formik.handleSubmit}
        labelCol={{
          span: 6,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        initialValues={{
          size: componentSize,
        }}
        onValuesChange={onFormLayoutChange}
        size={componentSize}
        style={{
          maxWidth: 600,
        }}
      >
        <Form.Item label="Hệ thống rạp">
          <Select onChange={handleChange}>{renderHeThongRapChieu()}</Select>
        </Form.Item>
        <Form.Item label="Cụm rạp">
          <Select onChange={onchangeField("maRap")}>
            {renderCumRapChieu()}
          </Select>
        </Form.Item>
        <Form.Item label="Ngày chiếu giờ chiếu">
          <DatePicker showTime onChange={onChange} />
        </Form.Item>
        <Form.Item label="Giá vé">
          <InputNumber
            min={75000}
            max={150000}
            defaultValue={75000}
            onChange={onchangeField("giaVe")}
          />
        </Form.Item>
        <Form.Item label="Chức năng">
          <button
            type="submit"
            className="inline-flex justify-center rounded-lg text-sm font-semibold py-3 px-4 bg-slate-900 text-white hover:bg-slate-700"
          >
            Tạo lịch chiếu
          </button>
        </Form.Item>
      </Form>
    </>
  );
}
