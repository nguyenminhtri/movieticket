/* eslint-disable react/jsx-pascal-case */
import { Tab } from "@headlessui/react";
import React from "react";
import { useDispatch } from "react-redux";
import Slider from "react-slick";
import {
  SET_PHIM_DANG_CHIEU,
  SET_PHIM_SAP_CHIEU,
} from "../../redux/types/Type";
import Film_Flip from "../Film/Film_Flip";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function MultipleRows(props) {
  const { arrPhim } = props;
  const dispatch = useDispatch();

  const renderPhim = () => {
    return arrPhim.map((phim, index) => {
      return (
        <Film_Flip key={index} className={`${["width-item"]}`} phim={phim} />
      );
    });
  };
  var settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    initialSlide: 0,
    row: 2,
    slidesPerRow: 2,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <div style={{ padding: "30px 10%" }}>
      <div className="mx-auto w-full max-w-md px-2 py-16 sm:px-0">
        <Tab.Group>
          <Tab.List className="flex space-x-1 rounded-xl bg-blue-900/20 p-1">
            <Tab
              onClick={() => {
                dispatch({ type: SET_PHIM_SAP_CHIEU });
              }}
              className={({ selected }) =>
                classNames(
                  "w-full rounded-lg py-2.5 text-sm font-medium leading-5 text-blue-700",
                  "ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2",
                  selected
                    ? "bg-white shadow"
                    : "text-blue-100 hover:bg-white/[0.12] hover:text-white"
                )
              }
            >
              Phim sắp chiếu
            </Tab>
            <Tab
              onClick={() => {
                dispatch({ type: SET_PHIM_DANG_CHIEU });
              }}
              className={({ selected }) =>
                classNames(
                  "w-full rounded-lg py-2.5 text-sm font-medium leading-5 text-blue-700",
                  "ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2",
                  selected
                    ? "bg-white shadow"
                    : "text-blue-100 hover:bg-white/[0.12] hover:text-white"
                )
              }
            >
              Phim đang chiếu
            </Tab>
          </Tab.List>
        </Tab.Group>
      </div>
      <Slider {...settings}>{renderPhim()}</Slider>
    </div>
  );
}
