import { useSelector } from "react-redux";
import ClipLoader from "react-spinners/ClipLoader";
export default function Loading() {
  const { isLoading } = useSelector((state) => state.LoadingReducer);

  return isLoading ? (
    <div
      style={{
        position: "fixed",
        top: 0,
        left: 0,
        height: "100%",
        width: "100%",
        backgroundColor: "rgba(0,0,0,0.5",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        zIndex: "10",
      }}
    >
      <ClipLoader color="#36d7b7" />
    </div>
  ) : (
    <></>
  );
}
