/* eslint-disable jsx-a11y/heading-has-content */
import React from "react";

export default function Film(props) {
  const { phim } = props;

  return (
    <div>
      <div className="m-1 h-full bg-gray-100 bg-opacity-75 px-8 pt-16 pb-24 rounded-lg overflow-hidden text-center relative">
        <div
          style={{ backgroundImage: `url(${phim.hinhAnh})` }}
          className="leading-relaxed mb-3 bg-center bg-cover"
        >
          <img src={phim.hinhAnh} alt="" className="w-full h-full opacity-0" />
        </div>
        <h1 className="title-font sm:text-2xl text-xl font-medium text-gray-900 mb-3">
          {phim.tenPhim}
        </h1>
        {/* <p>{phim.moTa}</p>  */}
      </div>
    </div>
  );
}
