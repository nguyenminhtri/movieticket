/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import { NavLink } from "react-router-dom";
import "./Film_Flip.css";
import { PlayCircleOutlined } from "@ant-design/icons";
import { useState } from "react";
import { Modal } from "antd";
export default function Film_Flip(props) {
  const { phim } = props;

  const [open, setOpen] = useState(false);
  return (
    <>
      <div className="page-content">
        <div
          className="card"
          style={{
            backgroundImage: `url(${phim.hinhAnh})`,
            backgroundSize: "cover",
            objectFit: "contain",
          }}
        >
          <div className="content ">
            <div className="play_button" onClick={() => setOpen(true)}>
              <PlayCircleOutlined />
            </div>

            <h2 className="title">{phim.tenPhim}</h2>

            <NavLink to={`/detail/${phim.maPhim}`} className="copy">
              {phim.moTa.length >= 60 ? (
                <p>
                  {phim.moTa.slice(0, 60)} ...
                  <span className="text-violet-500">(Xem thêm)</span>
                </p>
              ) : (
                phim.moTa
              )}
            </NavLink>
            <NavLink className="inline-flex justify-center rounded-lg text-sm font-semibold py-3 px-4 bg-green-900 text-white hover:bg-green-700">
              Đặt vé
            </NavLink>
          </div>
        </div>
      </div>
      <Modal
        centered
        open={open}
        onOk={() => setOpen(false)}
        onCancel={() => setOpen(false)}
        width={800}
        footer={null}
        className="px-auto bg-black"
      >
        <iframe
          width="720"
          height="400"
          src={phim.trailer}
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
          allowfullscreen
        ></iframe>
      </Modal>
    </>
  );
}
